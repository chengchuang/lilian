package com.histone.lilian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LilianApplication {

	public static void main(String[] args) {
		SpringApplication.run(LilianApplication.class, args);
	}

}
