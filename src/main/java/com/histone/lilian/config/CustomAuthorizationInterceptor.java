package com.histone.lilian.config;

import cn.dev33.satoken.stp.StpUtil;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.context.MagicUser;
import org.ssssssss.magicapi.core.interceptor.Authorization;
import org.ssssssss.magicapi.core.interceptor.AuthorizationInterceptor;

import javax.servlet.http.HttpServletRequest;

/**
 * 自定义用户名密码登录
 */
@Component
public class CustomAuthorizationInterceptor implements AuthorizationInterceptor {

    /**
     * 配置是否需要登录
     */
    @Override
    public boolean requireLogin() {
        return false;
    }

    /**
     * 是否拥有页面按钮的权限
     */
    @Override
    public boolean allowVisit(MagicUser magicUser, HttpServletRequest request, Authorization authorization) {
        if (StpUtil.hasRole("admin")) {
            return true;
        }
        switch (authorization) {
            case VIEW:
                return StpUtil.hasPermission("magic:api:list");
            case DELETE:
                return StpUtil.hasPermission("magic:api:remove");
            default:
                return StpUtil.hasPermission("magic:api:edit");
        }
    }

}
