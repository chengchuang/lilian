package com.histone.lilian.magic;

import cn.dev33.satoken.stp.StpUtil;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

import java.util.List;

/**
 * 代指当前登录的用户
 * @author histone
 */
@Component
@MagicModule("me")
public class MeFunctions {

    @Comment("获取userId")
    public String id() {
        return StpUtil.getLoginIdAsString();
    }

    @Comment("获取登录账号")
    public String userName() { return StpUtil.getSession().getString("userName"); }

    @Comment("是否是管理员")
    public Boolean isAdmin() {
        return StpUtil.hasRole("admin");
    }

    @Comment("获取权限列表")
    public List<String> perms() {
        return StpUtil.getPermissionList();
    }

    @Comment("获取角色列表")
    public List<String> roles() {
        return StpUtil.getRoleList();
    }

}
