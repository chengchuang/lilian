package com.histone.lilian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.histone.lilian.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    @Select("select role_value from sys_role where id in (" +
            "select role_id from sys_user_role where user_id = #{userId})")
    List<String> getRoleList(Object userId);

    //    @Select({
//            "<script>",
//            "select perms from sys_menu where menu_id in (select menu_id from sys_role_menu where role_id in ",
//            "<foreach collection='roleIdList' item='item' open='(' separator=',' close=')'>",
//            "#{item}",
//            "</foreach>",
//            ")</script>"
//    })
    @Select("select permission from sys_menu where id in (" +
                "select menu_id from sys_role_menu where role_id in (" +
                    "select role_id from sys_user_role where user_id = #{userId}" +
                ")" +
            ")")
    List<String> getPermissionList(Object userId);
}
