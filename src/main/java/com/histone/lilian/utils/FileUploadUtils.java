package com.histone.lilian.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class FileUploadUtils {
    /**
     * 文件上传
     * @param baseDir       上传目录
     * @param file          上传文件
     * @return              上传文件所在路径
     * @throws IOException
     */
    public static String upload(String baseDir, MultipartFile file) throws IOException {
        // 文件拓展名
        String extName = FileUtil.extName(file.getOriginalFilename());
        String fileName = IdUtil.fastSimpleUUID() + StrUtil.DOT + extName;
        File target = FileUtil.file(baseDir, fileName);

        if (!target.exists()) {
            if (!target.getParentFile().exists())
            {
                target.getParentFile().mkdirs();
            }
        }

        file.transferTo(target);

        return target.getName();
    }
}
