var MAGIC_EDITOR_CONFIG = {
    title: 'magic-api',
    defaultTheme: 'default',
    defaultExpand: true,
    checkUpdate: true,      // 不检查升级版本
    title: 'lilian-admin-api',
    header: {
        skin: true,    // 屏蔽皮肤按钮
        document: true,    // 屏蔽文档按钮
        repo: false,    // 屏蔽gitee和github
        qqGroup: false  // 屏蔽加入QQ群
    },
    editorFontFamily: 'JetBrainsMono, Consolas, "Courier New",monospace, 微软雅黑',
    // 其它配置参考本页中其它配置项
}