<p align="center">
	<img alt="logo" src="https://histoneup.gitee.io/lilian-doc/logo-mini.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Lilian Admin</h1>
<h4 align="center">在浏览器中就能coding的Java低代码快速开发框架</h4>
<p align="center">
	<a href="https://gitee.com/histoneUp/lilian/stargazers"><img src="https://gitee.com/histoneUp/lilian/badge/star.svg?theme=dark"></a>
	<a href="https://gitee.com/histoneUp/lilian"><img src="https://img.shields.io/badge/LilianAdmin-v2.0.0-brightgreen.svg"></a>
	<a href="https://gitee.com/histoneUp/lilian/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

> 有同学希望直接整合若依，这样功能更丰富。好的，安排！  [YuXi-Admin](https://gitee.com/histoneUp/yu-xi-admin)  
> 新项目链接 https://gitee.com/histoneUp/yu-xi-admin

## 平台简介

动态脚本（magic api）➕ 后台管理 ➕ 轻量权限（Sa-Token）   
基于magic api 动态脚本，在浏览器中即可编辑所有接口，
修改后动态编译，无需重启，急速响应需求变更。  
增强代码生成功能，在线创建数据库表&字段。几分钟配置出增删改查模块，完全不用拷贝代码、重新部署。

* 前端采用Vue3、Vite、Vben。
* 后端采用Spring Boot、magic api、mysql。
* 权限认证使用Sa-Token，轻量级框架，中文文档，比 Spring Security 更易上手。

#### 软件架构
- SpringBoot2
- magic-api
- Sa-Token
- HuTool
- AntDesign Vue3

#### 开发环境
NodeJs 16+ （必需）  
yarn  
mysql 8  
jdk 8

#### 后端启动

1.  执行 doc/lilian.sql，初始化数据库
2.  修改 src/main/resources/application.yml 配置文件中的数据库连接信息

```
  datasource:
    url: jdbc:mysql://127.0.0.1:3306/lilian?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false
    username: lilian
    password: lilian520
```

3.  启动 LilianApplication.main()

#### 前端启动

1. `cd _web`
1. `yarn`
1. `yarn dev`

>浏览器打开 https://127.0.0.1:3100  访问系统页面  
>浏览器打开 https://127.0.0.1:3100/#/magic-api   编辑接口脚本

#### 系统特色

> What❓ 整天都是增删改查，浪费时间又没有成就感  

用我们系统，增删改查无需改动任何代码，前端配置一下就完事

> What❓ 只是简单改动个字段，就要前端后端一通改动

用我们系统，在线创建数据库表、修改字段信息，无需其他操作就可以使页面随之变化

> What❓ 产品经理又要改逻辑，还要求十分钟搞定  WTF🐴

快来尝试下吧，在线修改脚本，改完就生效，还等啥呢
#### 内置功能
* 代码生成：在线创建数据库表，修改表字段，构造表单。自动生成菜单目录、增删改查等接口，前端通用CRUD页面模板
* 在线接口：在浏览器中查看编辑所有后台接口，动态编译，无需重启，实时发布。
* 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
* 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
* 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
* 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
* 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
* 登录日志：系统登录日志记录查询包含登录异常。

#### 在线体验

演示地址：[https://lilian.flyingdeep.top/](https://lilian.flyingdeep.top/)  
文档地址：[https://histoneup.gitee.io/lilian-doc](https://histoneup.gitee.io/lilian-doc/)

#### QQ 交流群
![aa](https://histoneup.gitee.io/lilian-doc/lilian-admin-qq-group1.png)
![bb](./doc/images/image1.png)

#### 致谢
- [magic-api](https://gitee.com/ssssssss-team/magic-api) 基于Java的接口快速开发框架  
- [Vben-admin](https://vvbin.cn/doc-next/) 一个开箱即用的前端框架
- [Sa-Token](https://gitee.com/dromara/sa-token) 一个轻量级 Java 权限认证框架，让鉴权变得简单、优雅！
- [Hutool](https://hutool.cn/docs/#/) 小而全的Java工具类库


