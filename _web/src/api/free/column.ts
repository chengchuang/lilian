import { defHttp } from '/@/utils/http/axios';
import { ColumnListGetResultModel, ColumnListItem, ColumnParams } from './model/freeModel';

const url = '/free/column';

// 查询动态表字段列表
export function getColumnListByPage(params?: ColumnParams) {
  return defHttp.get<ColumnListGetResultModel>({ url, params });
}

// 新增动态表字段
export function addColumn(data: ColumnListItem) {
  return defHttp.post<string>({ url, data });
}

// 修改动态表字段
export function updateColumn(data: ColumnListItem) {
  return defHttp.put<string>({ url, data });
}

// 修改动态表字段 不改动数据库表结构
export function updateColumnSimple(data: { id: string; status?: string }) {
  return defHttp.put<string>({ url: url + '/simple', data });
}

// 删除动态表字段
export function removeColumn(id: string) {
  return defHttp.delete<string>({ url: url + '/' + id });
}
