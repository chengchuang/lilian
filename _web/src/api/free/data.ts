import { defHttp } from '/@/utils/http/axios';
import qs from 'qs';
import { DataListGetResultModel, FreeDataParams } from '/@/api/free/model/freeModel';

const url = '/freeData/';

export const getFreeDataList = (params: FreeDataParams) =>
  defHttp.get<DataListGetResultModel>({ url: url + params.tableName
      + '?' + qs.stringify(params, { indices: false })
  });

export const addFreeData = (tableName: string, data) =>
  defHttp.post<string>({ url: url + tableName, data });

export const updateFreeData = (tableName: string, data) =>
  defHttp.put<string>({ url: url + tableName, data });

// 删除 多个id使用逗号隔开
export const removeFreeData = (tableName: string, ids: string) =>
  defHttp.delete<string>({ url: url + tableName + '/' + ids });
// 导入表
// 导出
