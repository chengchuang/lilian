import { defHttp } from '/@/utils/http/axios';
import { TableListGetResultModel, TableListItem } from './model/freeModel';

enum Api {
  Table = '/tool/gen/table',
  Crud = '/tool/gen/crud',
  Menu = '/tool/gen/menu',
}

const url = '/free/table';
// 查询所有动态表
export const getAllTableList = () => {
  return defHttp.get<Array<TableListItem>>({ url: url + '/list' });
};

// 表详情 基本信息、字段、字段字典
export const getTableDetail = (tableName: string) => {
  return defHttp.get<TableListItem>({ url: url + '/' + tableName });
};

// 新增动态表
export const addTable = (data: TableListItem) => {
  return defHttp.post<string>({ url, data });
};
export const updateTable = (data: TableListItem) => defHttp.put<string>({ url, data });

export const removeTable = (tableName: string) =>
  defHttp.delete<string>({ url: `${url}/${tableName}` });

// 生成数据库表
export function genTable(data?: TableListItem) {
  return defHttp.post<string>({ url: Api.Table, data });
}

// 生成增删改查接口
export function genCrud(data?: TableListItem) {
  return defHttp.post({ url: Api.Crud, data });
}

// 生成菜单
export function genMenu(data?: TableListItem) {
  return defHttp.post({ url: Api.Menu, data });
}
