import { defHttp } from '/@/utils/http/axios';
import { DictListItem } from './model/freeModel';
import { EditRecordRow } from '/@/components/Table';

const url = '/free/column/dict';

// 查询所有字典值
export function getAllDictList(columnId: string) {
  const params = { columnId };
  return defHttp.get<DictListItem[]>({ url, params });
}

// 新增动态表字段
export function saveDictList(data: EditRecordRow) {
  return defHttp.post<string>({ url, data });
}

// 设为默认
export function updateDict(data) {
  return defHttp.put<string>({ url, data });
}

// 删除动态表字段
export function removeDict(id: string) {
  return defHttp.delete<string>({ url: url + '/' + id });
}
