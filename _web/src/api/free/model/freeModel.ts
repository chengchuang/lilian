import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
export type TableParams = BasicPageParams & {
  key?: string;
  status?: string;
};

export type ColumnParams = {
  tableName: string;
  key?: string;
  status?: string;
};

export type FreeDataParams = BasicPageParams & {
  tableName: string;
};

export class TableTask {
  private status: string;
  private readonly msgInit: string;
  msg: string;
  icon: string;
  constructor(msg: string) {
    this.msg = this.msgInit = msg;
    this.icon = '';
    this.status = '';
    this.setStatus('loading');
  }
  setStatus(status: 'loading' | 'finish' | 'error') {
    if (this.status === status) return;
    this.status = status;
    switch (this.status) {
      case 'loading':
        this.msg = '正在' + this.msgInit + '...';
        this.icon = 'eos-icons:bubble-loading';
        break;
      case 'finish':
        this.msg = this.msgInit + '成功';
        this.icon = 'ant-design:check-circle-outlined';
        break;
      case 'error':
        this.msg = this.msgInit + '失败';
        this.icon = 'ant-design:warning-outlined';
        break;
    }
  }
  getStatus() {
    return this.status;
  }
}

export interface TableListItem {
  tableName: string;
  tableLabel: string;
  dataStructure: string;
  createBy: string;
  createTime: string;
  remark: string;
  status: number;
  columns?: ColumnListItem[];
}

export interface ColumnListItem {
  options: any[];
  id: string;
  code: string;
  name: string;
  orderNo: string;
  tableName: string;
  htmlType: string;
  required: boolean;
  maxLength: number;
  queryType: string;
  listShow: boolean;
  formShow: boolean;
  createTime: string;
  remark: string;
  status: number;
}

export interface FreeDataItem {
  id: string;
}

export interface DictListItem {
  id: string;
  dictLabel: string;
  dictValue: string;
  isDefault: string;
  remark: string;
}

/**
 * @description: Request list return value
 */
export type TableListGetResultModel = BasicFetchResult<TableListItem>;

export type ColumnListGetResultModel = BasicFetchResult<ColumnListItem>;

export type DataListGetResultModel = BasicFetchResult<FreeDataItem>;
