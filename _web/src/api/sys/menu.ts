import { defHttp } from '/@/utils/http/axios';
import { getMenuListResultModel } from './model/menuModel';
import { MenuParams, MenuListItem } from './model/systemModel';

const url = '/system/menu';

/**
 * @description: Get user menu based on id
 */

export const getMenuList = () => {
  return defHttp.get<getMenuListResultModel>({ url: url + '/route' });
};

export const getMenuTree = (params?: MenuParams) => {
  return defHttp.get({ url: url + '/tree', params });
};

export const addMenu = (data?: MenuListItem) => {
  return defHttp.post<string>({ url, data });
};

export const updateMenu = (data?: MenuListItem) => {
  return defHttp.put<string>({ url, data });
};

export const removeMenu = (id: string) => {
  return defHttp.delete<string>({ url: `${url}/${id}` });
};
