import { defHttp } from '/@/utils/http/axios';
import { LoginParams, LoginResultModel, GetUserInfoModel } from './model/userModel';
import { AccountListItem, AccountListGetResultModel } from './model/systemModel';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  Login = '/login',
  Logout = '/logout',
  GetUserInfo = '/getUserInfo',
  GetPermCode = '/getPermCode',
  TestRetry = '/testRetry',
  User = '/system/user',
  Profile = '/profile',
}

/**
 * @description: user login api
 */
export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: getUserInfo
 */
export function getUserInfo() {
  return defHttp.get<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}

export function getPermCode() {
  return defHttp.get<string[]>({ url: Api.GetPermCode });
}

export function doLogout() {
  return defHttp.get({ url: Api.Logout });
}

export function testRetry() {
  return defHttp.get(
    { url: Api.TestRetry },
    {
      retryRequest: {
        isOpenRetry: true,
        count: 5,
        waitTime: 1000,
      },
    },
  );
}

export function getAccountList(params: AccountListItem) {
  return defHttp.get<AccountListGetResultModel>({ url: Api.User, params });
}

export function addAccount(data: AccountListItem) {
  return defHttp.post<string>({ url: Api.User, data });
}

export function updateAccount(data: AccountListItem) {
  return defHttp.put<string>({ url: Api.User, data });
}

export function exportAccount(params: Recordable) {
  return defHttp.get<AccountListItem[]>({ url: Api.User + '/export', params });
}

export function removeAccount(id: string) {
  return defHttp.delete<string>({ url: Api.User + '/' + id });
}

export function getUserRoleIdList(userId: string) {
  return defHttp.get<Array<Number>>({ url: Api.User + '/role?userId=' + userId });
}

// 查询我的基本信息
export function getMyInfo() {
  return defHttp.get<GetUserInfoModel>({ url: Api.Profile });
}

// 查询我的基本信息
export function updateMyInfo(data: GetUserInfoModel) {
  return defHttp.put<string>({ url: Api.Profile, data });
}

export function changePwd(pwdOld: string, pwdNew: string) {
  const data = {
    pwdOld,
    pwdNew,
  };
  return defHttp.put<string>({ url: Api.Profile + '/changePwd', data });
}

export function resetPwd(userId: string) {
  const data = { id: userId };
  return defHttp.put<string>({ url: Api.User + '/resetPwd', data });
}

export function disableUser(userId: string) {
  const data = { id: userId };
  return defHttp.put<string>({ url: Api.User + '/kickout', data });
}
