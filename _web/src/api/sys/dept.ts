import { DeptListItem, DeptListGetResultModel } from './model/systemModel';
import { defHttp } from '/@/utils/http/axios';

const url = '/system/dept';

export const getDeptList = (params?: DeptListItem) =>
  defHttp.get<DeptListGetResultModel>({ url, params });

export const getDeptTree = (params?: DeptListItem) => defHttp.get({ url: `${url}/tree`, params });

export const addDept = (data: DeptListItem) => defHttp.post<String>({ url, data });

export const updateDept = (data: DeptListItem) => defHttp.put<String>({ url, data });

export const removeDept = (id: String) => defHttp.delete<String>({ url: `${url}/${id}` });
