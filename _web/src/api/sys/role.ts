import { RoleListItem, RolePageListGetResultModel } from './model/systemModel';
import { defHttp } from '/@/utils/http/axios';

const url = '/system/role';

export const getRoleListByPage = (params?: RoleListItem) =>
  defHttp.get<RolePageListGetResultModel>({ url, params });

export const getAllRoleList = (params?: RoleListItem) =>
  defHttp.get<RolePageListGetResultModel>({ url: url + '/list', params });

export const getRoleMenuList = (roleId: string) =>
  defHttp.get<RolePageListGetResultModel>({ url: url + '/menu?roleId=' + roleId });

export const addRole = (data: RoleListItem) => defHttp.post<string>({ url, data });

export const updateRole = (data: RoleListItem) => defHttp.put<string>({ url, data });

export const updateRoleStatus = (id: string, status: string) => {
  const data = {
    id,
    status,
  };
  return defHttp.put<string>({ url: `${url}/status`, data });
};

export const removeRole = (id: string) => defHttp.delete<string>({ url: `${url}/${id}` });
