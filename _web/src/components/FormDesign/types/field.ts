export interface Field {
  label: string;
  key: string;
  id: string;
  type: 'input' | 'radio' | 'select' | 'date';
}

export const iconMap = {
  input: 'input',
};
