export interface Rules {
  trigger: string
  enum: string
  len?: number
  max?: number
  message: string
  min?: number
  pattern: string
  required: boolean
  type: string
}

export interface WidgetForm {
  list: any[]
}


export const widgetForm: WidgetForm = {
  list: [],
}

export const basicComponents = [
  {
    label: '单行文本',
    type: 'input',
    key: 15,
  },
  {
    label: '密码框',
    type: 'password',
    key: 1,
  },
  {
    label: '多行文本',
    type: 'textarea',
    key: 2
  },
  {
    label: '计数器',
    type: 'number',
    key: 3
  },
  {
    label: '单选框组',
    type: 'radio',
    key: 4,
    options: {
      options: [
        {
          value: 'Option 1',
          label: 'Option 1'
        },
        {
          value: 'Option 2',
          label: 'Option 2'
        },
      ]
    }
  },
  {
    label: '多选框组',
    type: 'checkbox',
    key: 5,
    options:{
      options: [
        {
          label: 'Option 1',
          value: 'Option 1'
        },
        {
          label: 'Option 2',
          value: 'Option 2'
        },
      ],
    }
  },
  {
    label: '时间选择器',
    type: 'time',
    key: 6,
  },
  {
    label: '日期选择器',
    type: 'date',
    key: 7,
  },
  {
    label: '评分',
    type: 'rate',
    key: 8,
  },
  {
    label: '下拉选择框',
    type: 'select',
    key: 9,
    options: {
      options: [
        {
          label: 'Option 1',
          value: 'Option 1'
        },
        {
          label: 'Option 2',
          value: 'Option 2'
        },
      ],
    }
  },
  {
    label: '开关',
    type: 'switch',
    key: 10,
  },
  {
    label: '滑块',
    type: 'slider',
    key: 11
  }
]

