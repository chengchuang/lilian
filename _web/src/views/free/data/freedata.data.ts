import { ColumnListItem } from '/@/api/free/model/freeModel';
import {BasicColumn, FormSchema} from '/@/components/Table';
export function buildColumns(columns: any[]) {
  return columns
    .filter((item) => item.listShow)
    .map((item) => {
      const col: BasicColumn = {
        title: item.name,
        dataIndex: item.code,
      };
      if (item.htmlType === 'radio' || item.htmlType === 'select') {
        col.customRender = ({ text }) => {
          return item.options.find((option) => option.value === text)?.label || text;
        };
      }
      return col;
    });
}
export function buildSearchFormScheme(columns: ColumnListItem[]) {
  return columns
    .filter((item) => item.queryType)
    .map((item) => {
      const searchItem: FormSchema = {
        field: item.code,
        label: item.name,
        colProps: { span: 8 },
        component: 'Input',
      };
      switch (item.htmlType) {
        case 'radio':
        case 'select':
          searchItem.component = 'Select';
          searchItem.componentProps = {
            options: item.options,
            mode: item.queryType === 'BETWEEN' ? 'multiple' : '',
          };
          break;
        case 'date':
          if (item.queryType === 'BETWEEN') {
            searchItem.component = 'RangePicker';
          } else {
            searchItem.component = 'DatePicker';
          }
          break;
      }

      return searchItem;
    });
}

export function buildFormScheme(columns: ColumnListItem[]) {
  const result = columns
    .filter((item) => item.formShow)
    .map((item) => {
      const formItem: FormSchema = {
        field: item.code,
        label: item.name,
        required: !!item.required,
        component: 'Input',
      };
      switch (item.htmlType) {
        case 'radio':
        case 'select':
          formItem.component = 'Select';
          formItem.componentProps = {
            options: item.options,
          };
          break;
        case 'date':
          formItem.component = 'DatePicker';
          break;
        case 'textarea':
          formItem.component = 'InputTextArea';
          break;
      }

      return formItem;
    });
  result.push({
    field: 'id',
    label: 'id',
    show: false,
    component: 'Input',
  });
  return result;
}
