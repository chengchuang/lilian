import { BasicColumn, FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Checkbox } from 'ant-design-vue';
import { updateColumnSimple } from '/@/api/free/column';
import { useMessage } from '/@/hooks/web/useMessage';
import { formatT } from '/@/utils/dateUtil';

const htmlTypeOptions = [
  {
    label: '输入框',
    value: 'input',
    key: '1',
  },
  {
    label: '数字',
    value: 'number',
    key: '2',
  },
  {
    label: '单选',
    value: 'radio',
    key: '3',
  },
  {
    label: '多选',
    value: 'select',
    key: '4',
  },
  {
    label: '日期',
    value: 'date',
    key: '5',
  },
];

export const columns: BasicColumn[] = [
  {
    title: '字段名称',
    dataIndex: 'name',
    width: 200,
  },
  {
    title: '字段标识',
    dataIndex: 'code',
    width: 200,
  },
  {
    title: '排序',
    dataIndex: 'orderNo',
    width: 50,
  },
  {
    title: '表单类型',
    dataIndex: 'htmlType',
    width: 100,
    customRender: ({ text }) => {
      return htmlTypeOptions.find((item) => item.value === text)?.label;
    },
  },
  {
    title: '必填',
    dataIndex: 'required',
    width: 50,
    customRender: ({ record }) => {
      return h(Checkbox, {
        checked: record.required,
        onChange({ target }) {
          const { createMessage } = useMessage();
          const { checked = false } = target;
          const data = {
            id: record.id,
            required: checked,
          };
          updateColumnSimple(data)
            .then(() => {
              record.required = checked;
              createMessage.success(`已成功修改字段必填属性`);
            })
            .catch(() => {
              createMessage.error('修改字段必填属性失败');
            });
        },
      });
    },
  },
  {
    title: '列表页显示',
    dataIndex: 'listShow',
    width: 50,
    customRender: ({ record }) => {
      return h(Checkbox, {
        checked: record.listShow,
        onChange({ target }) {
          const { createMessage } = useMessage();
          const { checked = false } = target;
          const data = {
            id: record.id,
            listShow: checked,
          };
          updateColumnSimple(data)
            .then(() => {
              record.listShow = checked;
              createMessage.success(`已成功修改字段列表可见性`);
            })
            .catch(() => {
              createMessage.error('修改字段列表可见性失败');
            });
        },
      });
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
    format: formatT,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];
export const searchFormSchema: FormSchema[] = [
  {
    field: 'tableName',
    label: '数据表',
    component: 'Input',
    show: false,
    colProps: { span: 8 },
  },
  {
    field: 'key',
    label: '关键字',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    show: false,
    component: 'Input',
  },
  {
    field: 'tableName',
    label: '所属表',
    show: false,
    component: 'Input',
  },
  {
    field: 'name',
    label: '字段名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'code',
    label: '字段标识',
    component: 'Input',
    componentProps: {
      placeholder: '字母小写，下划线分隔。如 user_name',
    },
    rules: [
      { required: true, message: '请输入字段标识', trigger: 'blur' },
      {
        pattern: new RegExp('^[a-z][0-9a-z_]*[0-9a-z]$'),
        message: '请以小写字母开头，可包含小写字母、数字、下划线，但不要下划线结尾',
        len: 50,
        whitespace: false,
        trigger: 'blur',
      },
    ],
  },
  {
    field: 'maxLength',
    label: '最大长度',
    defaultValue: 32,
    colProps: {
      span: 8,
    },
    component: 'InputNumber',
    required: true,
  },
  {
    field: 'queryType',
    label: '查询方式',
    component: 'Select',
    colProps: {
      offset: 3,
      span: 8,
    },
    componentProps: {
      options: [
        {
          label: '相等',
          value: 'EQ',
          key: '1',
        },
        {
          label: '模糊',
          value: 'LIKE',
          key: '2',
        },
        {
          label: '范围',
          value: 'BETWEEN',
          key: '3',
        },
      ],
    },
  },
  {
    field: 'orderNo',
    label: '排序',
    defaultValue: 100,
    colProps: {
      span: 8,
    },
    component: 'InputNumber',
    required: true,
  },
  {
    field: 'listShow',
    label: '列表页显示',
    component: 'RadioButtonGroup',
    defaultValue: true,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
  {
    field: 'formShow',
    label: '表单页显示',
    component: 'RadioButtonGroup',
    defaultValue: true,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
  {
    field: 'htmlType',
    label: '表单类型',
    component: 'Select',
    defaultValue: 'input',
    ifShow: ({ values }) => values.formShow,
    colProps: {
      span: 8,
    },
    componentProps: {
      options: htmlTypeOptions,
    },
  },
  {
    field: 'required',
    label: '必填',
    component: 'RadioButtonGroup',
    defaultValue: true,
    ifShow: ({ values }) => values.formShow,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
];

export const dictColumns: BasicColumn[] = [
  {
    title: 'id',
    dataIndex: 'id',
    ifShow: false,
  },
  {
    title: 'Dict Label',
    dataIndex: 'dictLabel',
    editRow: true,
  },
  {
    title: 'Dict Value',
    dataIndex: 'dictValue',
    editRow: true,
  },
  {
    title: '备注',
    dataIndex: 'remark',
    editRow: true,
  },
];
