import { FormSchema } from '/@/components/Table';
import { TableTask } from '/@/api/free/model/freeModel';

export const searchFormSchema: FormSchema[] = [
  {
    field: 'roleName',
    label: '角色名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '停用', value: '1' },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'tableLabel',
    label: '表描述',
    required: true,
    component: 'Input',
  },
  {
    field: 'tableName',
    label: '表名',
    component: 'Input',
    componentProps: {
      placeholder: '字母小写，下划线分隔。如 sys_user',
    },
    rules: [
      { required: true, message: '请输入表名' },
      {
        pattern: new RegExp('^[a-z][0-9a-z_]*[0-9a-z]$'),
        message: '请以小写字母开头，可包含小写字母、数字、下划线，但不要下划线结尾',
        len: 50,
        whitespace: false,
        trigger: 'blur',
      },
    ],
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: '0',
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '停用', value: '1' },
      ],
    },
  },
  {
    field: 'dataStructure',
    label: '数据结构',
    component: 'RadioButtonGroup',
    defaultValue: 'simple',
    ifShow: false,
    componentProps: {
      options: [
        { label: '单表', value: 'simple' },
        { label: '树表', value: 'tree' },
      ],
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
];

export const taskList: TableTask[] = [
  new TableTask('创建数据库表'),
  new TableTask('编写增删改查等接口'),
  new TableTask('生成菜单'),
];
